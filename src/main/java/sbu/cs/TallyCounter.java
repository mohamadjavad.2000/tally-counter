package sbu.cs;

public class TallyCounter implements TallyCounterInterface {
	private int counter;
	TallyCounter (){
		counter=0;
	}
    @Override
    public void count() {
    	if (9999!=counter)
    		counter++;
    }

    @Override
    public int getValue() {
        return counter;
    }

    @Override
    public void setValue(int value) throws IllegalValueException {
    	if (value <0 || value>9999) {
    		throw new IllegalValueException();
    	}
    	counter=value;
    }
}
