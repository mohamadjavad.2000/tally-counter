package sbu.cs;

public class ExerciseLecture4 {

    /*
     *   implement a function that returns factorial of given n
     *   lecture 4 page 15
     */
	public long factorial(int n) throws IllegalValueException {
	    long nFactorial = 1;
	    if (n < 0) {
	      throw new IllegalValueException();
	    }
	    for (int i = 1; i <= n; i++)
	      nFactorial *= i;
	    return nFactorial;
	  }

	  /*
	   * implement a function that return nth number of fibonacci series the series ->
	   * 1, 1, 2, 3, 5, 8, ... lecture 4 page 19
	   */
	  public long fibonacci(int n) throws IllegalValueException {
	    if (n <0) {
	      throw new IllegalValueException();
	    }
	    long nth=0, a =0, b =1;
	    
	    for (int i = 1; i < n; i++) {
	      nth = a + b;
	      a = b;
	      b = nth;
	    }
	    if (n==1) {
	    	return 1;
	    }
	    return nth;
	  }

    /*
     *   implement a function that return reverse of a given word
     *   lecture 4 page 19
     */
    public String reverse(String word)  {
        return  new StringBuilder(word).reverse().toString();
    }

    /*
     *   implement a function that returns true if the given line is
     *   palindrome and false if it is not palindrome.
     *   palindrome is like 'wow', 'never odd or even', 'Wow'
     *   lecture 4 page 19
     */
    public boolean isPalindrome(String line) {
    	String enil =reverse(line).replaceAll(" ", "").toLowerCase();
        return line.replaceAll(" ", "").toLowerCase().equals(enil) ;
    }

    /*
     *   implement a function which computes the dot plot of 2 given
     *   string. dot plot of hello and ali is:
     *       h e l l o
     *   h   *
     *   e     *
     *   l       * *
     *   l       * *
     *   o           *
     *   lecture 4 page 26
     */
    public char[][] dotPlot(String str1, String str2) {
    	char ch [][]=new char [str1.length()][];
    	int row=0,col=0;
         for( ; row < str1.length() ; row ++){
        	 ch[row]=new char [str2.length()];
             for( col = 0 ; col < str2.length() ; col ++){
                 if(str1.charAt(row) == str2.charAt(col)){;
                	 ch[row][col]='*';
                 }
                 else{
                	 ch[row][col]=' ';
                 }
             }
         }
        return ch;
    }
}
