package sbu.cs;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

public class ExerciseLecture6 {

    /*
     *   implement a function that takes an array of int and return sum of
     *   elements at even positions
     *   lecture 6 page  16
     */
    public long calculateEvenSum(int[] arr) {
    	long sum=0;
    	for ( int i=0 ; i<arr.length;i+=2) {
    		sum+=arr[i];
    	}
        return sum;
    }

    /*
     *   implement a function that takes an array of int and return that
     *   array in reverse order
     *   lecture 6 page 16
     */
    public int[] reverseArray(int[] arr) {
    	int temp=0;
    	int size=arr.length;
    	for ( int i=0 ; i<size/2;i++) {
    		temp=arr[size-i-1];
    		arr[size-i-1]=arr[i];
    		arr[i]=temp;
    	}
        return arr;
    }

    /*
     *   implement a function that calculate product of two 2-dim matrices
     *   lecture 6 page 21
     */
    public double[][] matrixProduct(double[][] m1, double[][] m2) throws RuntimeException {
    	double ans[][]=new double [m1.length][];
    	for ( int i=0 ;i<m1.length;i++) {
    		ans[i]=new double [m2.length];
    		for ( int j=0 ;j<m2[j].length;j++) {
    			ans[i][j]=0;
    			for ( int k=0 ;k<m2.length;k++) {
    				ans[i][j]+=m1[i][k]*m2[k][j];
    			}
    		}
    	}
        return ans;
    }

    /*
     *   implement a function that return array list of array list of string
     *   from a 2-dim string array
     *   lecture 6 page 30
     */
    public List<List<String>> arrayToList(String[][] names) {
    	List<List<String>> listOFLists = new ArrayList<>() ;
    	for ( int i=0 ;i<names.length;i++) {
    		List<String> array = new ArrayList<>();
    		for ( int j=0 ;j<names[0].length;j++) {
        		array.add(names[i][j]);
        	}
    		listOFLists.add(array);
    	}
    	
        return listOFLists;
    }

    /*
     *   implement a function that return a list of prime factor of integer n
     *   in ascending order
     *   lecture 6 page 30
     */
    public List<Integer> primeFactors(int n) {
    	List<Integer> list= new ArrayList<>();
    	int i=2;
    	while (n>1) {
    		if (n%i==0) {
    			while (n%i==0) {
    				n/=i;
    			}
    			list.add(i);
    		}
    		i++;
    	}
        return list;
    }

    /*
     *   implement a function that return a list of words in a given string
     *   lecture 6 page 30
     */
    public List<String> extractWord(String line) {
    	List<String> list= new ArrayList<String>();
    	Pattern pattern=Pattern.compile("([a-zA-Z])\\w+");
    	Matcher matcher=pattern.matcher(line);
    	while (matcher.find()) {
    		line =matcher.group();
    		System.out.println(line);
    		list.add(line);
    		
    	}
        return list;
    }
}
