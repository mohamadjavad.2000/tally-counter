package sbu.cs;

public class ExerciseLecture5 {

    /*
     *   implement a function to create a random password with
     *   given length using lower case letters
     *   lecture 5 page 14
     */
    public String weakPassword(int length) throws  IllegalValueException{
    	 if (length < 0) {
   	      throw new IllegalValueException();
   	    }
    	StringBuilder pass=new StringBuilder();
    	for (int i=0 ;i<length;i++) {
    		pass.append((char)(Math.random()*26+97));
    	}
        return pass.toString();
    }

    /*
     *   implement a function to create a random password with
     *   given length and at least 1 digit and 1 special character
     *   lecture 5 page 14
     */
    public String strongPassword(int length) throws Exception , IllegalValueException {
    	if (length < 3) {
     	      throw new IllegalValueException();
     	    }
    	String charList="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    	String specialCharList ="!@#$%^&*()_+=-/\\.,<>?|\";:~`][{}\n\t\r\b";
    	StringBuilder pass=new StringBuilder();
    	int rand;
    	int specialCharSize,intSize;
    	specialCharSize=(int)(Math.random()*(length-2)+1);
    	intSize=(int)(Math.random()*(length-specialCharSize-1)+1);
    	for (int i=0 ;i<length;i++) {
    		rand=(int) (Math.random()*3);
    		if (rand==2 && specialCharSize>0) {
    			pass.append(specialCharList.charAt((int)(Math.random()*specialCharList.length())));
    		}
    		else if (rand==1 && intSize>0) {
    			pass.append((int)(Math.random()*10));
    		}
    		else {
    			pass.append(charList.charAt((int)(Math.random()*52)));
    		}
    	}
        return pass.toString();
    }

    /*
     *   implement a function that checks if a integer is a fibobin number
     *   integer n is fibobin is there exist an i where:
     *   //    n = fib(i) + bin(fib(i))
     *   where fib(i) is the ith fibonacci number and bin(i) is the number
     *   of ones in binary format
     *   lecture 5 page 17
     */
    public boolean isFiboBin(int n) {
    	ExerciseLecture4 ins=new ExerciseLecture4();
    	for (int i=0 ;i<=n;i++) {
    		if  (ins.fibonacci(i)+bin(ins.fibonacci(i))==n) {
    			return true;
    		}
    	}
        return false;
    }

	private long bin(long fibonacci) {
		long ones=0;
		while(fibonacci>0) {
			if (fibonacci%2==1) {
				ones++;
			}
			fibonacci/=2;
		}
		return ones;
	}
}
